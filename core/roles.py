from enum import Enum


class Role(Enum):
    Administrator = 0,
    Moderator = 1,
    Member = 2

    def __int__(self) -> int:
        return self.value[0]

    def __str__(self) -> str:
        return self.name


# class Role:
#     def __init__(self, role_enum: Roles) -> None:
#         self.role = role_enum
#
#     def __repr__(self) -> str:
#         return str(self.role)
#
#     def __str__(self) -> str:
#         return str(self.role.name)
#
#     def __int__(self) -> int:
#         return self.role.value[0]
