from db.repository import IRepository


def add_user_to_database(repository: IRepository,
                         tg_id: int, chat_id: int, name: str, link: str, link_name: str) -> None:
    """ Add user or update info into repository. """
    repository.update_user(tg_id=tg_id, chat_id=chat_id, name=name, link=link, link_name=link_name)
