from telegram.ext import ApplicationBuilder, PicklePersistence, AIORateLimiter
from db.utils import create_repository
import logging

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

with open('token.txt', 'r') as file:
    token = file.readline().strip()
    #persistence = PicklePersistence(filepath='.picklepersistence')
    repository = create_repository('training.db')
    # app = ApplicationBuilder().token(token).persistence(persistence=persistence).rate_limiter(AIORateLimiter()).build()
    app = ApplicationBuilder().token(token).rate_limiter(AIORateLimiter()).build()
    bot = app.bot
    job_queue = app.job_queue
