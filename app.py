from config import app, repository
from handlers.silenthandlers import SilentHandler
from handlers.schedulehandler import ScheduleHandler
from handlers.invitehandler import InviteHandler


if __name__ == '__main__':
    invite = InviteHandler(repository)
    schedule = ScheduleHandler(repository)
    app.add_handler(SilentHandler(repository).handler(), -1)
    app.add_handler(schedule.handler(entry_cmd='schedule'))
    app.add_handler(invite.handler(entry_cmd='enter'))
    app.run_polling()


