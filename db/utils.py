import os
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from db.repository import DbRepository, IRepository
from db.base import BaseRepository
from db.models import SqlOrmBase


def create_repository(path: str) -> IRepository:
    if not os.path.exists(path):
        open(path, 'w+').close()

    engine = create_engine('sqlite:///' + path)
    SqlOrmBase.metadata.create_all(engine)
    session = Session(bind=engine)
    repo = DbRepository(BaseRepository(session, engine))
    return repo
