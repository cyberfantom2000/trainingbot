from typing import Optional
from dataclasses import dataclass


@dataclass
class EventFilter:
    id: Optional[int] = None
    weekday: Optional[int] = None
    schedule_id: Optional[int] = None


@dataclass
class ScheduleFilter:
    id: Optional[int] = None
    owner_id: Optional[int] = None
    weekday: Optional[int] = None
    invite_key: Optional[str] = None


@dataclass
class UserFilter:
    id: Optional[int] = None
    ids: Optional[list[int]] = None
    member_status: Optional[int] = None
