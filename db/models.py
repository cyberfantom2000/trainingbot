from sqlalchemy import Column, Integer, BigInteger, String, Table, DateTime, ForeignKey, Time
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

SqlOrmBase = declarative_base()

event_association_table = Table(
    'event_association_table',
    SqlOrmBase.metadata,
    Column('user_id', ForeignKey('users.id'), primary_key=True),
    Column('event_id', ForeignKey('events.id'), primary_key=True)
)

schedule_association_table = Table(
    'schedule_association_table',
    SqlOrmBase.metadata,
    Column('user_id', ForeignKey('users.id'), primary_key=True),
    Column('schedule_id', ForeignKey('schedules.id'), primary_key=True)
)


class User(SqlOrmBase):
    """ User ORM model """
    __tablename__ = 'users'
    id = Column(BigInteger, primary_key=True)  # telegram id
    member_status = Column(Integer, default=2)
    chat_id = Column(BigInteger, nullable=False)
    name = Column(String)
    link = Column(String)
    link_name = Column(String)


class Notice(SqlOrmBase):
    """ Notice ORM model. """
    __tablename__ = 'notices'
    id = Column(Integer, primary_key=True)
    chat_id = Column(BigInteger, nullable=False)
    msg_id = Column(BigInteger, nullable=False)
    text = Column(String)


class Event(SqlOrmBase):
    """ Event ORM model """
    __tablename__ = 'events'
    id = Column(Integer, primary_key=True)
    active_member_ids = Column(BigInteger, ForeignKey('users.id'))  # список участников
    active_members = relationship('User', foreign_keys=active_member_ids)
    not_active_member_ids = Column(BigInteger, ForeignKey('users.id'))
    not_active_members = relationship('User', foreign_keys=not_active_member_ids)
    owner_id = Column(BigInteger, ForeignKey('users.id'))
    owner = relationship('User', foreign_keys=[owner_id], uselist=False)
    date = Column(DateTime, nullable=False)
    schedule_id = Column(BigInteger, ForeignKey('schedules.id'))
    schedule = relationship('Schedule', foreign_keys=[schedule_id], uselist=False)
    notice_id = Column(Integer, ForeignKey('notices.id'))
    notices = relationship('Notice', foreign_keys=[notice_id], cascade='all, delete')


class Schedule(SqlOrmBase):
    __day_name = ('dummy', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье')
    """ Event schedule ORM model """
    __tablename__ = 'schedules'
    id = Column(Integer, primary_key=True)
    owner_id = Column(BigInteger, ForeignKey('users.id'))
    owner = relationship('User', foreign_keys=[owner_id], uselist=False,
                         backref=backref('owned_schedules', cascade='all, delete'))
    place = Column(String, nullable=False)
    weekday = Column(Integer, nullable=False)  # 1 monday, 7 sunday
    event_time = Column(DateTime, nullable=False)
    invite_key = Column(String, nullable=False)
    members = relationship('User', secondary=schedule_association_table, backref='schedules')
    duration = Column(String, default='2  hour')

    def __str__(self) -> str:
        return f'Место: {self.place}\nПериодичность: {self.event_time.strftime("%H:%M")} - ' \
               f'{self.__day_name[self.weekday]}\nДлительность: {self.duration}'
