from abc import ABC, abstractmethod
from db.models import Event, Schedule, User, Notice
from db.base import BaseRepository, date_for_next_weekday
from datetime import datetime, time
from db.filters import EventFilter, ScheduleFilter, UserFilter


class IRepository(ABC):
    """ Repository interface """

    @abstractmethod
    def update(self, orm) -> bool or None:
        pass

    @abstractmethod
    def create_event(self, weekday: int, members: list[int], schedule_id: int, owner_id: int) -> Event or None:
        pass

    @abstractmethod
    def remove_event(self, event_id: int) -> bool or None:
        pass

    @abstractmethod
    def events(self, filters: EventFilter = None) -> list[Event]:
        pass

    @abstractmethod
    def update_event(self, event_id: int, day: int = None, event_time: time = None,
                     place: str = None, duration: str = None) -> Event or None:
        pass

    @abstractmethod
    def create_user(self, tg_id: int, chat_id: int, name: str, link: str = '',
                    link_name: str = '', member_status: int = 2) -> User or None:
        pass

    @abstractmethod
    def remove_user(self, tg_id: int) -> bool or None:
        pass

    @abstractmethod
    def users(self, filters: UserFilter = None) -> list[User]:
        pass

    @abstractmethod
    def update_user(self, tg_id: int, chat_id: int = None, name: str = None, link: str = None,
                    link_name: str = None, member_status: int = None) -> User | None:
        pass

    @abstractmethod
    def create_schedule(self, place: str, weekday: int, event_time: datetime,
                        owner_id: int, invite_token: str, duration: str = '') -> Schedule or None:
        pass

    @abstractmethod
    def remove_schedule(self, schedule_id: int) -> bool or None:
        pass

    @abstractmethod
    def schedules(self, filters: ScheduleFilter = None) -> list[Schedule]:
        pass

    @abstractmethod
    def update_schedule(self, schedule_id: int, place: str = None, weekday: str = None,
                        event_time: time = None, duration: str = None) -> Schedule or None:
        pass

    @abstractmethod
    def schedule_remove_member(self, schedule_id: int, tg_id: int) -> bool or None:
        pass

    @abstractmethod
    def create_notice(self, chat_id: int, msg_id: int) -> Notice:
        pass

    @abstractmethod
    def remove_notice(self, notice_id: int) -> bool or None:
        pass


class DbRepository(IRepository):
    def __init__(self, base: BaseRepository) -> None:
        self.base = base

    def update(self, orm) -> bool:
        return self.base.add_and_commit(orm)

    def create_event(self, weekday: int, members: list[int], schedule_id: int, owner_id: int) -> Event or None:
        event = Event(weekday=weekday, not_active_member_ids=members,
                      schedule_id=schedule_id, owner_id=owner_id)

        return event if self.base.add_and_commit(event) else None

    def remove_event(self, event_id: int) -> bool or None:
        return self.base.remove(Event, event_id)

    def events(self, filters: EventFilter = None) -> list[Event]:
        filter_list = None
        if filters:
            filter_list = []
            if filters.id is not None:
                filter_list.append(Event.id == filters.id)
            if filters.weekday is not None:
                filter_list.append(Event.weekday == filters.weekday)
            if filters.schedule_id:
                filter_list.append(Event.schedule_id == filters.schedule_id)
        return self.base.get(Event, filter_list)

    def update_event(self, event_id: int, day: int = None, event_time: time = None,
                     place: str = None, duration: str = None) -> Event or None:
        events = self.events(EventFilter(id=event_id))
        if not events:
            return None
        event = events[0]
        if place:
            event.place = place
        if duration:
            event.duration = duration
        if event_time:
            event.datetime = datetime.combine(event.datetime.date(), event_time)
        if day:
            date = date_for_next_weekday(day)
            event.datetime = datetime.combine(date, event.datetime.time())
        self.base.add_and_commit(event)
        return event

    def create_user(self, tg_id: int, chat_id: int, name: str, link: str = '',
                    link_name: str = '', member_status: int = 2) -> User or None:
        user = User(id=tg_id, chat_id=chat_id, name=name, link=link, link_name=link_name, member_status=member_status)
        return user if self.base.add_and_commit(user) else None

    def remove_user(self, tg_id: int) -> bool or None:
        return self.base.remove(User, tg_id)

    def users(self, filters: UserFilter = None) -> list[User]:
        filter_list = None
        if filters:
            filter_list = []
            if filters.id is not None:
                filter_list.append(User.id == filters.id)
            if filters.ids:
                for i in filters.ids:
                    filter_list.append(User.id == i)
            if filters.member_status is not None:
                filter_list.append(User.member_status == filters.member_status)
        return self.base.get(User, filter_list)

    def update_user(self, tg_id: int, chat_id: int = None, name: str = None, link: str = None,
                    link_name: str = None, member_status: int = 2) -> User | None:
        filters = UserFilter(id=tg_id)
        users = self.users(filters)
        if not users:
            if self.create_user(tg_id, chat_id, name, link, link_name, member_status) is None:
                return None
            users = self.users(filters)
            return users[0] if users else None

        user = users[0]
        if chat_id:
            user.chat_id = chat_id
        if name:
            user.name = name
        if link:
            user.link = link
        if link_name:
            user.link_name = link_name
        if member_status:
            user.member_status = member_status

        return user if self.base.add_and_commit(user) else None

    def create_schedule(self, place: str, weekday: int, event_time: datetime,
                        owner_id: int, invite_token: str, duration: str = '') -> Schedule or None:
        schedule = Schedule(place=place, weekday=weekday, event_time=event_time, invite_key=invite_token,
                            owner_id=owner_id, duration=duration)
        return schedule if self.base.add_and_commit(schedule) else None

    def remove_schedule(self, schedule_id: int) -> bool or None:
        return self.base.remove(Schedule, schedule_id)

    def schedules(self, filters: ScheduleFilter = None) -> list[Schedule]:
        filter_list = None
        if filters:
            filter_list = []
            if filters.id is not None:
                filter_list.append(Schedule.id == filters.id)
            if filters.weekday is not None:
                filter_list.append(Schedule.weekday == filters.weekday)
            if filters.owner_id is not None:
                filter_list.append(Schedule.owner_id == filters.owner_id)
            if filters.invite_key is not None:
                filter_list.append(Schedule.invite_key == filters.invite_key)
        return self.base.get(Schedule, filter_list)

    def update_schedule(self, schedule_id: int, place: str = None, weekday: int = None,
                        event_time: time = None, duration: str = None) -> Schedule or None:
        schedules = self.schedules(ScheduleFilter(id=schedule_id))
        if not schedules:
            return None

        schedule = schedules[0]
        if place:
            schedule.place = place
        if weekday:
            if 0 < weekday < 8:
                schedule.weekday = weekday
            else:
                return None
        if event_time:
            schedule.time = event_time
        if duration:
            schedule.duration = duration

        return schedule if self.base.add_and_commit(schedule) else None

    def schedule_remove_member(self, schedule_id: int, tg_id: int) -> bool or None:
        users = self.users(UserFilter(id=tg_id))
        schedules = self.schedules(ScheduleFilter(id=schedule_id))

        if not users or not schedules:
            return None

        schedule = schedules[0]
        user = users[0]
        if user in schedule.members:
            schedule.members.remove(user)
        return self.base.add_and_commit(schedule)

    def create_notice(self, chat_id: int, msg_id: int) -> Notice or None:
        notice = Notice(chat_id=chat_id, msg_id=msg_id)
        return notice if self.base.add_and_commit(notice) else None

    def remove_notice(self, notice_id: int) -> bool or None:
        return self.base.remove(Notice, notice_id)
