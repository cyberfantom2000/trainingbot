from datetime import datetime
from calendar import monthrange
from sqlalchemy.exc import PendingRollbackError


def catch_rollback_error(method):
    """ Catch SqlAlchemy PendingRollbackError and try call session.rollback(). """
    def wrapper(*args, **kwargs):
        try:
            return method(*args, **kwargs)
        except PendingRollbackError:
            args[0].session.rollback()
            return None
    return wrapper


class BaseRepository:
    def __init__(self, session, engine) -> None:
        self.session = session
        self.engine = engine

    @catch_rollback_error
    def add_and_commit(self, orm_model) -> bool:
        self.session.add(orm_model)
        self.session.commit()
        return True

    @catch_rollback_error
    def get(self, orm_type, filters=None) -> list:
        if filters:
            query = self.session.query(orm_type).filter(*filters)
        else:
            query = self.session.query(orm_type)
        return list(query)

    @catch_rollback_error
    def remove(self, orm_type, orm_id) -> bool:
        orm = self.session.query(orm_type).filter(orm_type.id == orm_id).first()
        if orm:
            self.session.delete(orm)
            self.session.commit()
            return True
        else:
            return False


def date_for_next_weekday(day: int) -> datetime:
    """ Get date for next weekday """
    now = datetime.now()
    _, days_in_month = monthrange(now.year, now.month)
    current_weekday = now.isoweekday()
    current_day = now.day
    current_month = now.month
    current_year = now.year

    while day != current_weekday:
        current_weekday += 1
        current_day += 1

        if current_day > days_in_month:
            current_day = 1
            current_month = current_month + 1 if current_month != 12 else 1
            current_year = current_year + 1 if current_month == 12 else current_year

        if current_weekday > 7:
            current_weekday = 1

    return datetime(current_year, current_month, current_day)
