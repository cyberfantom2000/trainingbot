from collections.abc import Iterable
from db.repository import IRepository
from db.filters import UserFilter, ScheduleFilter
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (ContextTypes, ConversationHandler, CommandHandler,
                          MessageHandler, CallbackQueryHandler, filters)
from core.roles import Role
from datetime import datetime
import secrets


(START_OVER, CREATE, UPDATE, REMOVE, CANCEL, MENU, EXIT,
 SAVE_INPUT, PLACE, TIME, DAY, DURATION, ACTION, CONFIRM,
 PREV, NEXT, SCROLL, SCROLL_ELEMENTS, CURRENT_ELEMENT,
 EDIT, SCROLL_TYPE, *other) = map(str, range(0, 30))


scroll_edit_buttons = [
            [InlineKeyboardButton(text='<', callback_data=PREV),
             InlineKeyboardButton(text='>', callback_data=NEXT)],
            [ # InlineKeyboardButton(text='Изменить', callback_data=UPDATE),
             InlineKeyboardButton(text='Удалить', callback_data=REMOVE)],
            [InlineKeyboardButton(text='Отмена', callback_data=CANCEL)]
        ]

scroll_exit_buttons = [
            [InlineKeyboardButton(text='<', callback_data=PREV),
             InlineKeyboardButton(text='>', callback_data=NEXT)],
            [InlineKeyboardButton(text='Покинуть', callback_data=EXIT)],
            [InlineKeyboardButton(text='Отмена', callback_data=CANCEL)]
        ]


class ScheduleHandler:
    """ This class implementation CRUD operations for schedule object. """
    __ask_input = {
        DAY: 'Напишите мне дни тренировок через запятую числами, где 1 - понедельник, а 7 - воскресенье...',
        TIME: 'Напишите мне время начала тренировки в формате чч.мм или чч:мм...',
        DURATION: 'Напишите мне длительность тренировки(например: 2 часа)...',
        PLACE: 'Напишите мне место где будет проходить тренировка...'
    }

    def __init__(self, repo: IRepository) -> None:
        self.repo = repo

    def handler(self, entry_msg: str = None, entry_cmd: str = None, stop_cmd: str = None) -> ConversationHandler:
        if not entry_msg and not entry_cmd:
            raise ValueError('One or more parameters of [entry_msg, entry_cmd] must be defined!')

        entry_points = []
        if entry_msg:
            entry_points.append(MessageHandler(filters.Regex('^{}$'.format(entry_msg)), self._init_conversation))
        if entry_cmd:
            entry_points.append(CommandHandler(entry_cmd, self._init_conversation))

        fallbacks = [CallbackQueryHandler(self.stop, pattern='^{}$'.format(CANCEL))]
        if stop_cmd:
            fallbacks.append(CommandHandler(stop_cmd, self.stop))

        handler = ConversationHandler(
            entry_points=entry_points,
            states={
                MENU: [
                    CallbackQueryHandler(self.start_new_schedule, pattern='^{}$'.format(CREATE)),
                    CallbackQueryHandler(self.start_scroll_edit_schedule, pattern='^{}$'.format(EDIT)),
                    CallbackQueryHandler(self.start_scroll_exit_schedule, pattern='^{}$'.format(EXIT)),
                    CallbackQueryHandler(self.stop, pattern='^{}$'.format(CANCEL)),
                ],
                SCROLL: [
                    CallbackQueryHandler(self.scroll_schedule, pattern='^{0}$|^{1}$'.format(PREV, NEXT)),
                    CallbackQueryHandler(self.update_schedule, pattern='^{}$'.format(UPDATE)),
                    CallbackQueryHandler(self.remove_schedule, pattern='^{}$'.format(REMOVE)),
                    CallbackQueryHandler(self.exit_from_schedule, pattern='^{}$'.format(EXIT)),
                    CallbackQueryHandler(self.start, pattern='^{}$'.format(CANCEL))
                ],
                CREATE: [
                    CallbackQueryHandler(self.ask_for_input, pattern='^{0}$|^{1}$|^{2}$|^{3}$'
                                         .format(PLACE, TIME, DAY, DURATION)),
                    CallbackQueryHandler(self.confirm_create, pattern='^{}$'.format(CONFIRM)),
                    CallbackQueryHandler(self.start, pattern='^{}$'.format(CANCEL))
                ],
                SAVE_INPUT: [MessageHandler(filters.TEXT & ~filters.COMMAND, self.save_input)],

            },
            fallbacks=fallbacks
        )

        return handler

    @staticmethod
    async def _init_conversation(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
        ScheduleHandler._clear_context(context)
        return await ScheduleHandler.start(update, context)

    @staticmethod
    async def start(update: Update, context: ContextTypes.DEFAULT_TYPE, text: str = '') -> str:
        text += '🗓 Расписание тренировок.\nВыберите действие:'
        buttons = [
            [InlineKeyboardButton(text='Создать', callback_data=CREATE)],
            [InlineKeyboardButton(text='Изменить', callback_data=EDIT)],
            [InlineKeyboardButton(text='Покинуть', callback_data=EXIT)],
            [InlineKeyboardButton(text='Выход', callback_data=CANCEL)]
        ]
        keyboard = InlineKeyboardMarkup(buttons)

        if context.user_data.get(START_OVER):
            await update.callback_query.answer()
            await update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        else:
            context.user_data[START_OVER] = True

            if update.message:
                await update.message.reply_text(text=text, reply_markup=keyboard)
            else:
                await update.callback_query.message.reply_text(text=text, reply_markup=keyboard)

        return MENU

    @staticmethod
    async def stop(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
        ScheduleHandler._clear_context(context)

        # TODO  удалять лишнии сообщения
        if update.callback_query:
            await update.callback_query.delete_message()
        else:
            await update.message.delete()
        return ConversationHandler.END

    @staticmethod
    async def start_new_schedule(update: Update, context: ContextTypes.DEFAULT_TYPE, msg: str = '') -> str:
        if msg:
            text = msg + '\n'
        else:
            text = ('Для нового элемента расписания потребуется ввести следующие параметры:\n'
                    'Место проведения, время, день недели, длительность(по умолчанию 2 часа).\n')

        text += 'Порядок ввода не важен, когда все поля будут заполнены нажмите создать!'

        buttons = [
            [
                InlineKeyboardButton(text='Место', callback_data=PLACE),
                InlineKeyboardButton(text='Время', callback_data=TIME),
                InlineKeyboardButton(text='День', callback_data=DAY),
            ],
            [InlineKeyboardButton(text='Длительность', callback_data=DURATION)],
            [InlineKeyboardButton(text='Создать', callback_data=CONFIRM),
             InlineKeyboardButton(text='Отмена', callback_data=CANCEL)]
        ]
        keyboard = InlineKeyboardMarkup(buttons)

        if context.user_data.get(START_OVER):
            await update.callback_query.answer()
            await update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        else:
            context.user_data[START_OVER] = True
            if update.message:
                await update.message.reply_text(text=text, reply_markup=keyboard)
            else:
                await update.callback_query.message.reply_text(text=text, reply_markup=keyboard)
        return CREATE

    async def confirm_create(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
        try:
            time = context.user_data[TIME].replace('.', ':')
            day = context.user_data[DAY]
            place = context.user_data[PLACE]
            duration = context.user_data.get(DURATION, '2 часа')
            invite_key = str(secrets.token_hex(16))
            if self.repo.create_schedule(place, day, datetime.strptime(time, '%H:%M'),
                                         update.effective_user.id, invite_key, duration):
                text = 'Элемент расписания успешно создан! ' \
                       'В сообщении выше прикреплен код для приглашения участников.\n'
                self._clear_context(context)
                await update.callback_query.answer()
                await update.callback_query.edit_message_text(invite_key)
            else:
                text = 'Ошибка создания элемента расписания, попробуйте снова!\n'
            return await self.start(update, context, text)
        except KeyError:
            time = context.user_data.get(TIME, '')
            day = context.user_data.get(DAY, '')
            place = context.user_data.get(PLACE, '')
            duration = context.user_data.get(DURATION, '2 часа')
            text = 'Поля: место, время, день недели - обязательны для заполнения. Ваши значения:\n' \
                   'Место: {0}, Время: {1}, День: {2}, Длительность: {3}.\n'.format(place, time, day, duration)
            return await self.start_new_schedule(update, context, text)

    async def start_scroll_edit_schedule(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str or int:
        user = self.repo.users(UserFilter(id=update.effective_user.id))
        if not user:
            return await self._send_msg_and_stop(update, context, 'Пользователь не найден, попробуйте снова!')

        # TODO: Доделать роль администратора и модератора
        user_schedules = self.repo.schedules(ScheduleFilter(owner_id=user.id))
        if not user_schedules:
            user_schedules = []
        schedule_list = {i: schedule for i, schedule in enumerate(user_schedules)}
        context.user_data[SCROLL_TYPE] = EDIT
        return await self.start_scroll_schedule(update, context, schedule_list)

    async def start_scroll_exit_schedule(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str or int:
        user = self.repo.users(UserFilter(id=update.effective_user.id))
        if not user:
            return await self._send_msg_and_stop(update, context, 'Пользователь не найден, попробуйте снова!')
        schedule_list = {i: schedule for i, schedule in enumerate(user.schedules)}
        context.user_data[SCROLL_TYPE] = EXIT
        return await self.start_scroll_schedule(update, context, schedule_list)

    @staticmethod
    async def start_scroll_schedule(update: Update, context: ContextTypes.DEFAULT_TYPE, schedule_list) -> str:
        if not schedule_list:
            text = 'У вас нет активных элементов расписания.'
            buttons = [[InlineKeyboardButton(text='Отмена', callback_data=CANCEL)]]
        else:
            act = context.user_data[SCROLL_TYPE]
            buttons = scroll_edit_buttons if act == EDIT else scroll_exit_buttons
            context.user_data[SCROLL_ELEMENTS] = schedule_list
            context.user_data[CURRENT_ELEMENT] = 0
            text = '0.\n' + str(schedule_list[0])

        keyboard = InlineKeyboardMarkup(buttons)
        await update.callback_query.answer()
        await update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SCROLL

    @staticmethod
    async def scroll_schedule(update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
        callback_data = update.callback_query.data
        elements = context.user_data[SCROLL_ELEMENTS]
        idx = context.user_data[CURRENT_ELEMENT]
        if callback_data == PREV:
            idx = len(elements) - 1 if idx == 0 else idx - 1
        else:
            idx = 0 if idx + 1 >= len(elements) else idx + 1

        context.user_data[CURRENT_ELEMENT] = idx
        text = str(idx) + '.\n' + str(elements[idx])
        act = context.user_data[SCROLL_TYPE]
        buttons = scroll_edit_buttons if act == EDIT else scroll_exit_buttons
        keyboard = InlineKeyboardMarkup(buttons)
        await update.callback_query.answer()
        await update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
        return SCROLL

    async def remove_schedule(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
        idx = context.user_data[CURRENT_ELEMENT]
        schedule = context.user_data[SCROLL_ELEMENTS].pop(idx)
        if self.repo.remove_schedule(schedule.id):
            text = 'Успешно удалено!\n'
        else:
            text = 'Ошибка удаления, попробуйте снова!\n'

        return await self.start(update, context, text)

    async def exit_from_schedule(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str or int:
        idx = context.user_data[CURRENT_ELEMENT]
        schedule = context.user_data[SCROLL_ELEMENTS].pop(idx)
        if self.repo.schedule_remove_member(schedule.id, update.effective_user.id):
            text = 'Вы больше не принимаете участие в этом событии!\n'
        else:
            text = 'Ошибка выхода из события, попробуйте еще раз!\n'

        return await self.start(update, context, text)

    async def update_schedule(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
        # TODO
        pass

    async def ask_for_input(self, update: Update, context: ContextTypes.DEFAULT_TYPE, text: str = '') -> str:
        try:
            if update.callback_query:
                context.user_data[ACTION] = update.callback_query.data
            action = context.user_data[ACTION]
            text += self.__ask_input[action]

            if context.user_data.get(START_OVER):
                await update.callback_query.answer()
                await update.callback_query.edit_message_text(text=text)
            else:
                await update.message.reply_text(text)
            return SAVE_INPUT
        except KeyError:
            text = 'Неизвестный параметр, что-то пошло не так... Свяжитесь с разработчиком или попробуйте снова!\n'
            return await self.start(update, context, text)

    async def save_input(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str:
        action = context.user_data[ACTION]
        msg = update.message.text
        if action == TIME:
            t = msg.replace('.', ':').strip().split(':')
            if len(t) == 2 and t[0].isdigit() and 0 <= int(t[0]) <= 23 and t[1].isdigit() and 0 <= int(t[1]) <= 59:
                msg = ':'.join(t)
                context.user_data[TIME] = msg
            else:
                context.user_data[START_OVER] = False
                text = 'Не корректный формат времени! Повторите попытку.\n'
                return await self.ask_for_input(update, context, text)
        elif action == DAY:
            if msg.isdigit() and 1 <= int(msg) <= 7:
                pass
            else:
                context.user_data[START_OVER] = False
                text = 'Не корректный ввод недели! Повторите попытку.\n'
                return await self.ask_for_input(update, context, text)
            context.user_data[DAY] = int(msg)
        else:
            context.user_data[action] = msg

        context.user_data[START_OVER] = False
        text = f'Запомнил: "{msg}".\n'
        return await self.start_new_schedule(update, context, text)

    async def _send_msg_and_stop(self, update: Update, context: ContextTypes.DEFAULT_TYPE, msg: str) -> int:
        if update.callback_query:
            await update.callback_query.edit_message_text(text=msg)
        else:
            await update.message.reply_text(text=msg)
        return await self.stop(update, context)

    @staticmethod
    def _clear_context(context: ContextTypes.DEFAULT_TYPE, keys: Iterable = None):
        if keys is None:
            keys = [DAY, TIME, DURATION, PLACE, START_OVER, SCROLL_ELEMENTS, CURRENT_ELEMENT, SCROLL_TYPE]
        for key in keys:
            if key in context.user_data:
                del context.user_data[key]
