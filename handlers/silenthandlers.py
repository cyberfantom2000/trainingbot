from telegram import Update
from telegram.ext import ContextTypes, TypeHandler
from core.common import add_user_to_database
from db.repository import IRepository


class SilentHandler:
    """ This handler call before other handler and don't stop other handler. """
    def __init__(self, repo: IRepository):
        self.repo = repo

    async def __call__(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        user = update.effective_user
        add_user_to_database(repository=self.repo, tg_id=user.id, chat_id=update.effective_chat.id,
                             name=user.full_name, link=user.link, link_name=user.name)

    def handler(self) -> TypeHandler:
        return TypeHandler(Update, self)

