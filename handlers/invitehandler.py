from db.repository import IRepository
from db.filters import ScheduleFilter, UserFilter
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (ContextTypes, ConversationHandler, CommandHandler,
                          MessageHandler, CallbackQueryHandler, filters)

SAVE_INPUT, CANCEL, END, DELETE_MSG = map(str, range(0, 4))


class InviteHandler:
    def __init__(self, repo: IRepository) -> None:
        self.repo = repo

    def handler(self, entry_msg: str = None, entry_cmd: str = None, stop_cmd: str = None) -> ConversationHandler:
        if not entry_msg and not entry_cmd:
            raise ValueError('One or more parameters of [entry_msg, entry_cmd] must be defined!')

        entry_points = []
        if entry_msg:
            entry_points.append(MessageHandler(filters.Regex(f'^{entry_msg}$'), self._init_conversation))
        if entry_cmd:
            entry_points.append(CommandHandler(entry_cmd, self._init_conversation))

        fallbacks = [CallbackQueryHandler(self.stop, pattern='^{}$'.format(CANCEL))]
        if stop_cmd:
            fallbacks.append(CommandHandler(stop_cmd, self.stop))

        handler = ConversationHandler(
            entry_points=entry_points,
            states={
                SAVE_INPUT: [
                    CallbackQueryHandler(self.stop, pattern=f'^{CANCEL}$'),
                    MessageHandler(filters.TEXT & ~filters.COMMAND, self.save_input)
                ]
            },
            fallbacks=fallbacks
        )
        return handler

    async def _init_conversation(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str or int:
        if context.args:
            token = context.args[0].rstrip()
            info = self._enter_to_schedule(token, update.effective_user.id)
            if info:
                text = f'Вы успешно добавлены в участники расписания:\n{info}'
            else:
                text = 'Тренировка с таким ключом не найдена! Попробуйте снова или используйте другой ключ.'
            await update.message.reply_text(text=text)
            return ConversationHandler.END
        else:
            context.user_data[DELETE_MSG] = None
            return await self.ask_for_input(update, context)

    @staticmethod
    async def ask_for_input(update: Update, context: ContextTypes.DEFAULT_TYPE, text: str = '') -> str:
        if not text:
            text = 'Отправьте мне пригласительный ключ и я добавлю вас в список участников...'
        keyboard = InlineKeyboardMarkup.from_button(InlineKeyboardButton(text='Отмена', callback_data=CANCEL))
        context.user_data[DELETE_MSG] = await update.message.reply_text(text=text, reply_markup=keyboard)
        return SAVE_INPUT

    async def save_input(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> str or int:
        token = update.message.text.rstrip()
        info = self._enter_to_schedule(token, update.effective_user.id)
        if info:
            text = f'Вы успешно добавлены в участники расписания:\n{info}'
            return await self.stop(update, context, text)
        else:
            context.user_data[DELETE_MSG].delete()
            return await self.ask_for_input(update, context, 'Тренировка с таким ключом не найдена! '
                                                             'Попробуйте снова или используйте другой ключ.')

    def _enter_to_schedule(self, token: str, user_id: int) -> str:
        schedules = self.repo.schedules(ScheduleFilter(invite_key=token))
        for schedule in schedules:
            if schedule.owner_id != user_id:
                users = self.repo.users(UserFilter(id=user_id))
                if not users:
                    break
                schedule.members.append(users[0])
                self.repo.update(schedule)
                return str(schedule)

        return ''

    @staticmethod
    async def stop(update: Update, context: ContextTypes.DEFAULT_TYPE, msg='') -> int:
        if update.callback_query:
            await update.callback_query.answer()
            if msg:
                await update.callback_query.edit_message_text(text=msg)
            else:
                await update.callback_query.delete_message()
        else:
            await update.message.reply_text(msg)

        context.user_data[DELETE_MSG] = None
        return ConversationHandler.END
