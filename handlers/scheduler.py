from datetime import datetime
from db.filters import ScheduleFilter, UserFilter, EventFilter
from db.models import Event, Schedule
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (ContextTypes, ConversationHandler, CommandHandler,
                          MessageHandler, CallbackQueryHandler, filters)

ACTIVE, NOACTIVE = map(str, range(0, 2))


class Scheduler:
    def __init__(self, bot, job_queue, repo, update_time: str = '7:00') -> None:
        """ Args:
         bot - PTB telegram bot,
         job_queue - PTB interface job_queue,
         repo - repository,
         update_time - time at format HH:MM daily update. """
        self.bot = bot
        self.job_queue = job_queue
        self.repo = repo
        self.activate_schedules()
        time = datetime.strptime(update_time, '%H:%M').time()
        self.job_queue.run_daily(self.activate_schedules, time)

    async def activate_schedules(self) -> None:
        """ Iterate schedules and create event. """
        day = datetime.now().isoweekday()
        next_day = 0 if day == 7 else day + 1
        for schedule in self.repo.schedules(ScheduleFilter(weekday=next_day)):
            await self.activate_schedule(schedule)

    async def activate_schedule(self, schedule) -> None:
        """ Create event for concrete schedule. """
        event = self.create_event(schedule)
        await self.primary_notify_users(event)
        self.repo.update(event)

    def create_event(self, schedule) -> Event:
        """ Create event from schedule. """
        member_ids = [member.id for member in schedule.members]
        return self.repo.create_event(schedule.weekday, member_ids, schedule.id, schedule.owner_id)

    async def primary_notify_users(self, event) -> None:
        """ Send new msg about event for all members. """
        member_ids = event.active_member_ids
        member_ids.extend(event.not_active_member_ids)
        members = self.repo.users(UserFilter(ids=member_ids))

        buttons = [[InlineKeyboardButton(text='+', callback_data=ACTIVE),
                    InlineKeyboardButton(text='-', callback_data=NOACTIVE)]]
        keyboard = InlineKeyboardMarkup(buttons)
        text = self._event_to_text(event)
        for member in members:
            msg = await self.bot.send_message(chat_id=member.chat_id, text=text, reply_markup=keyboard)
            notice = self.repo.create_notice(member.chat_id, msg.id, text=text)
            if notice:
                event.notices.append(notice)

    async def secondary_notify_users(self, event) -> None:
        """ Edit msg about event for all members. """
        pass

    @staticmethod
    def _event_to_text(event) -> str:
        text = (f'{event.id}\n'
                f'Скоро событие!\n'
                f'Дата: {event.date.strftime("$d.$m  %H:%M")}\n'
                f'Место: {event.schedule.place}\n'
                f'Продолжительность: {event.schedule.duration}\n'
                f'Уже учавствуют: {len(event.active_member_ids)} человек!\n'
                f'Примешь участие?\n')
        return text


class ScheduleHandler:
    def __init__(self, repo, bot, scheduler: Scheduler) -> None:
        self.repo = repo
        self.bot = bot
        self.scheduler = scheduler

    def handler(self) -> CallbackQueryHandler:
        return CallbackQueryHandler(self.handle, pattern=f'^{ACTIVE}$|^{NOACTIVE}$')

    async def update_counter(self, event) -> None:
        """ Update counter into msg for all members. """
        text = f'Уже учавствуют: {len(event.active_member_ids)} человек!\n'
        for notice in event.notices:
            rows = notice.text.split('\n')
            if rows[-2] != text:
                rows[-2] = text
                await self.bot.edit_message_text(text=''.join(rows), chat_id=notice.chat_id, message_id=notice.msg_id)

    async def handle(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        """ Handle status change of a member in an event. """
        cb = update.callback_query.data
        rows = update.callback_query.message.text.split('\n')
        event_id = int(rows[0].rstrip())
        event = self.repo.events(EventFilter(id=event_id))
        if not event:
            await update.callback_query.edit_message_text(
                'Похоже событие закончилось и это объявление больше не работает :(.')
        else:
            user_id = update.effective_user.id
            if cb == ACTIVE:
                if user_id not in event.active_member_ids:
                    event.active_member_ids.append(user_id)
                if user_id in event.not_active_member_ids:
                    event.not_active_member_ids.remove(user_id)
                rows[-2] = f'Уже учавствуют: {len(event.active_member_ids)} человек!\n'
                rows[-1] = 'Ты учавствуешь!\n'
            else:
                if user_id in event.active_member_ids:
                    event.active_member_ids.remove(user_id)
                if user_id not in event.not_active_member_ids:
                    event.not_active_member_ids.append(user_id)
                rows[-2] = f'Уже учавствуют: {len(event.active_member_ids)} человек!\n'
                rows[-1] = 'Примешь участие?\n'
            text = ''.join(rows)
            buttons = [[InlineKeyboardButton(text='+', callback_data=ACTIVE),
                        InlineKeyboardButton(text='-', callback_data=NOACTIVE)]]
            keyboard = InlineKeyboardMarkup(buttons)
            await update.callback_query.edit_message_text(text=text, reply_markup=keyboard)
            self.repo.updat(event)
            for notice in event.notices:
                if notice.chat_id == update.effective_chat.id and notice.msg_id == user_id:
                    notice.text = text
                    self.repo.update(notice)
                    break

            await self.update_counter(event)
